# NodeJs Simulation Services #

This file exists to allow git to recognize the directory structure of the workspace. Any documentation about this area should be placed in the main README.md file for the project, or, in the case of information about a specific sub-project, within the pertinent sub-project's documentation.

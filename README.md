# Project Calliope #

Greek Muse who presides over eloquence and epic poetry.

Project Calliope is the main workspace for all other projects.

This description will be expanded on as time goes by.

## Requirements and Setup Instructions ##
Download and install the latest version of NodeJs
Download and install the latest version of Git

At the command line, navigate to the /scripts directory within this project
Run: npm install
Run: node dev_mount_modules.js

This will retrieve, link, and install all needed repositories for development work.

## Structure ##
- **./DevelopmentUI/**: mount point for the static web server and static content that interfaces with Microservice APIs. This folder will not be exist in the project until the appropriate project has been mounted.
- **./lib/**: a folder containing the mount points for utility modules developed to support other internal projects.
- **./Microservices/**: a folder containing the mount points for the isolated services that constitute the logical portion of the application.
- **./scripts/**: contains the various scripts needed for initializing environments and deployment.
- **./SimulationServices/**: a folder containing the mount points for the isolated services that constitute the simulation portions of the application.

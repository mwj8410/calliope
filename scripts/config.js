var dir_sep = /^win/.test(process.platform) ? '\\' : '/';

module.exports = {
	NodeModules: {
		DevelopmentUI: {
			path: '',
			repo: 'git@bitbucket.org:mwj8410/developmentui.git',
			moduleDir: 'developmentui',
			links: []
		},
		// Lib Modules
		Microservice_Host: {
			path: 'lib' + dir_sep + 'node',
			repo: 'git@bitbucket.org:mwj8410/microservice_host.git',
			moduleDir: 'microservice_host',
			links: []
		},
		UtilityServices: {
			path: 'lib' + dir_sep + 'node',
			moduleDir: 'utilityservices',
			repo: 'git@bitbucket.org:mwj8410/utilityservices.git',
			links: []
		},
		
		// Microservices
		ConfigurationService: {
			path: 'Microservices',
			moduleDir: 'configurationservice',
			repo: 'git@bitbucket.org:mwj8410/configurationservice.git',
			links: ['Microservice_Host']
		},

		Data_Interface: {
			path: 'Microservices',
			moduleDir: 'datainterface',
			repo: 'git@bitbucket.org:mwj8410/datainterface.git',
			links: ['Microservice_Host']
		},
		Ptah: {
			path: 'Microservices',
			moduleDir: 'ptah',
			repo: 'git@bitbucket.org:mwj8410/ptah.git',
			links: ['Microservice_Host']
		},
		Pha_Authentication: {
			path: 'Microservices',
			moduleDir: 'pha_authentication',
			repo: 'git@bitbucket.org:mwj8410/pha_authentication.git',
			links: ['Microservice_Host']
		},

		// Simulation Services
		Janus: {
			path: 'SimulationServices',
			moduleDir: 'janus',
			repo: 'git@bitbucket.org:mwj8410/janus.git',
			links: []
		}
	}
};

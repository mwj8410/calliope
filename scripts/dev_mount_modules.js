/* global echo exit exec popd pushd which */
require('shelljs/global');

if (!which('git')) {
	echo('Sorry, this script requires git');
	exit(1);
}

// Set the working directory to the base directory
pushd('..'); // add base directory
pushd('+1'); // flip the directory stack
popd(); // remove the last entry from the directory stack.
// This leaves the base directory and the only directory in the stack.
var workspace_base_dir = process.cwd();
echo('\nBase Directory set.\n');


var config = require('./config'),
	hostedNodeModules = config.NodeModules,
	dir_sep = /^win/.test(process.platform) ? '\\' : '/';

Object.keys(hostedNodeModules).forEach(function(key) {

	echo('\n\n\n');
	echo('***** Begin ' + key + ' *****');
	echo('\n');
	if(hostedNodeModules[key].path.length > 0) {
		// push the path
		pushd(hostedNodeModules[key].path);
	}

	// Pull the repo
	exec('git clone '+ hostedNodeModules[key].repo);
	pushd(hostedNodeModules[key].moduleDir);

	// Apply links
	hostedNodeModules[key].links.forEach(function(link) {
		exec('npm link ' + workspace_base_dir + dir_sep + hostedNodeModules[link].path + dir_sep + hostedNodeModules[link].moduleDir);
	});

	// Install reqs
	exec('npm install');
	popd();

	if(hostedNodeModules[key].path.length > 0) {
		// go back to base path
		popd();
	}
});

exit(0);
